fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(data => {
    const films = data;

    films.forEach(film => {
      console.log(`Номер епізоду: ${film.episodeId}, назва фільму: ${film.name}, Короткий зміст: ${film.openingCrawl}`);
      
      film.characters.forEach(person => {
        fetch(person)
          .then(response => response.json())
          .then(personData => {
            console.log(`Персонаж: ${personData.name}, Стать: ${personData.gender}`);
          })
          .catch(error => console.error('Помилка при отриманні персонажу:', error));
      });
    });
  })
.catch(error => console.error('Помилка при отриманні фільму:', error));





